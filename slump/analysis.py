import json
import os

import cv2
import time
import imutils
import math

import numpy as np
from typing import *
from dataclasses import dataclass, asdict, field
from PIL import Image
from pathlib import Path
from picamera2 import MappedArray
from .camera import Camera, save_image
from pprint import pprint
from timeit import default_timer as timer
from scipy.interpolate import interp1d
from numpy.polynomial import Polynomial, Chebyshev, Legendre, Laguerre, Hermite, HermiteE

@dataclass
class AnalysisResult:
    height_px: float  # Height of the slump in pixels
    height_in: float       # Height of the slump in inches
    width_px: float   # Width of the slump in pixels
    width_in: float        # Width of the slump in inches
    # angles: List[float] # List of angles of the slump sides
    maximum_point: Tuple[float, float]  # Maximum point of the slump curve
    roughness: float    # Roughness of the slump curve
    timestamp: float    # Timestamp of the image
    contours: List[List[Tuple[float, float]]] = field(default_factory=list)  # List of contours found in the image
    center_pt: Tuple[float, float] = field(default_factory=tuple)  # Center point of the slump


@dataclass
class AnalysisSettings:
    low_hue: int = 88
    high_hue: int = 100
    low_sat: int = 24
    high_sat: int = 255
    low_val: int = 0
    high_val: int = 255

    threshold_min: int = 0
    threshold_max: int = 255

    erosions: int = 10
    dilations: int = 0

    kernel_mode: int = 3
    kernel_size: int = 3
    
    height_poly: List[float] = field(default_factory=list)
    width_poly: List[float] = field(default_factory=list)
    
    height_cheby: List[float] = field(default_factory=list)
    height_legendre: List[float] = field(default_factory=list)
    height_laguerre: List[float] = field(default_factory=list)
    height_hermite: List[float] = field(default_factory=list)
    height_hermiteE: List[float] = field(default_factory=list)

    def __getitem__(self, item):
        return getattr(self, item)
    
    def __setitem__(self, key, value):
        setattr(self, key, value)

    @property
    def __dict__(self):
        return asdict(self)

    @property
    def json(self):
        return json.dumps(self.__dict__, indent=4)

    @classmethod
    def from_json(cls, json_string: str):
        return cls(**json.loads(json_string))


class Kernel:
    def __init__(self, kernel_size: int = 3):
        self.kernel_size = kernel_size
        self.horizontal_size = None
        self.vertical_size = None
        self.square_size = None
        self.cross_size = None
        # self.ellipse_size = None

        self.horizontal = self.get_horizontal(self.kernel_size)
        self.horizontal_size = self.kernel_size

        self.vertical = self.get_vertical(self.kernel_size)
        self.vertical_size = self.kernel_size

        self.square = self.get_square(self.kernel_size)
        self.square_size = self.kernel_size

        self.cross = self.get_cross(self.kernel_size)
        self.cross_size = self.kernel_size

        # ERROR: Ellipse breaks for some reason
        # self.ellipse = self.get_ellipse(self.kernel_size)
        # self.ellipse_size = self.kernel_size

    def get_horizontal(self, kernel_size: int = None):
        if kernel_size is None:
            kernel_size = self.kernel_size

        if kernel_size == self.horizontal_size:
            return self.horizontal

        self.horizontal_size = kernel_size
        return cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, 1))

    def get_vertical(self, kernel_size: int = None):
        if kernel_size is None:
            kernel_size = self.kernel_size

        if kernel_size == self.vertical_size:
            return self.vertical

        self.vertical_size = kernel_size
        return cv2.getStructuringElement(cv2.MORPH_RECT, (1, kernel_size))

    def get_square(self, kernel_size: int = None):
        if kernel_size is None:
            kernel_size = self.kernel_size

        if kernel_size == self.square_size:
            return self.square

        self.square_size = kernel_size
        return cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, kernel_size))

    def get_cross(self, kernel_size: int = None):
        if kernel_size is None:
            kernel_size = self.kernel_size

        if kernel_size == self.cross_size:
            return self.cross

        self.cross_size = kernel_size
        return cv2.getStructuringElement(cv2.MORPH_CROSS, (kernel_size, kernel_size))

    # ERROR: Ellipse breaks for some reason
    # def get_ellipse(self, kernel_size: int = None):
    #     if kernel_size is None:
    #         kernel_size = self.kernel_size
    #
    #     if kernel_size == self.ellipse_size:
    #         return self.ellipse
    #
    #     self.ellipse_size = kernel_size
    #     return cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernel_size, kernel_size))

    def get_from_mode(self, mode: int, kernel_size: int = None):
        if kernel_size is None:
            kernel_size = self.kernel_size
        if mode == 0:
            return self.get_horizontal(kernel_size)
        elif mode == 1:
            return self.get_vertical(kernel_size)
        elif mode == 2:
            return self.get_square(kernel_size)
        elif mode == 3:
            return self.get_cross(kernel_size)
        # ERROR: Ellipse breaks for some reason
        # elif mode == 4:
        #     return self.get_ellipse(kernel_size)
        else:
            raise ValueError(f'Invalid mode {mode}')

class SlumpAnalyzer:
    def __init__(self, camera: Camera = None, frame_size: Tuple[int, int] = (2592, 1944)):
        self.camera = camera
        self.analysis_results: List[AnalysisResult] = []
        self.set_frame_size(frame_size)

        self.kernel = Kernel().get_from_mode(3)
        self.settings = AnalysisSettings()
        self.settings.height_poly = [-0.151, 0.0022, -0.0000005]
        self.height_mode = 'poly'

        settings_path = Path(__file__).parent / 'analysis_settings.json'
        if os.path.exists(settings_path):
            print(f'Loading settings from {settings_path}...')

            with open(settings_path, 'r') as f:
                self.settings = AnalysisSettings.from_json(f.read())
            self.set_settings(self.settings)
            pprint(self.settings.__dict__)
        else:
            print(f'No settings file found at {settings_path}')


    def __getitem__(self, item):
        return getattr(self, item)
    
    def __setitem__(self, key, value):
        setattr(self, key, value)


    def set_frame_size(self, frame_size: Tuple[int, int]):
        self.width_interp = interp1d([0, frame_size[0]], [0, 2592])
        self.height_interp = interp1d([0, frame_size[1]], [0, 1944])

    def set_camera(self, camera: Camera):
        self.camera = camera
        
    def get_settings(self) -> AnalysisSettings:
        return self.settings
        
    def set_settings(self, settings: AnalysisSettings):
        self.settings = settings
        self.kernel = Kernel(self.settings.kernel_size).get_from_mode(self.settings.kernel_mode)
        self.height_poly = np.polynomial.Polynomial(self.settings.height_poly)
        self.height_cheby = Chebyshev(self.settings.height_cheby)
        self.height_laguerre = Laguerre(self.settings.height_laguerre)
        self.height_legendre = Legendre(self.settings.height_legendre)
        self.height_hermite = Hermite(self.settings.height_hermite)
        self.height_hermiteE = HermiteE(self.settings.height_hermiteE)

    def save_settings(self):
        with open('analysis_settings.json', 'w') as f:
            f.write(self.settings.json)

    def get_data(self):
        # Provide the newest analysis result
        # Also provide the trajectory of the slump within a given time period
        pass

    def get_trajectory(self, time_period: float):
        # Get the trajectory of the slump within a given time period
        pass

    def purge_data(self, time_period: float, purge_all: bool = False, save_to_file: bool = False):
        # Purge data older than a given time period
        # If purge_all is True, purge all data
        # If save_to_file is True, save the data to a file
        pass

    def set_height_mode(self, mode: str):
        if mode not in ['poly', 'cheby', 'laguerre', 'legendre', 'hermite', 'hermiteE']:
            raise ValueError(f'Invalid height mode {mode}')
        self.height_mode = mode

    def height_px_to_in(self, height_px: float):
        # return self.settings.height_poly[2] * height_px ** 2 + self.settings.height_poly[1] * height_px + self.settings.height_poly[0]
        interp_height_px = float(self.height_interp(height_px))
        height_in = 0
        if self.height_mode == 'poly':
            height_in = self.height_poly(interp_height_px)
        elif self.height_mode == 'cheby':
            height_in = self.height_cheby(interp_height_px)
        elif self.height_mode == 'laguerre':
            height_in = self.height_laguerre(interp_height_px)
        elif self.height_mode == 'legendre':
            height_in = self.height_legendre(interp_height_px)
        elif self.height_mode == 'hermite':
            height_in = self.height_hermite(interp_height_px)
        elif self.height_mode == 'hermiteE':
            height_in = self.height_hermiteE(interp_height_px)
        else:
            raise ValueError(f'Invalid height mode {self.height_mode}')
        
        return height_in
    
    def width_px_to_in(self, width_px: float, height_in: float):
        return self.settings.width_poly[0] * width_px + self.settings.width_poly[1] * height_in

    def single_image_analysis(self, image_array: np.array):
        img = image_array.copy()
        analysis_result = AnalysisResult(-1, -1, -1, -1, (-1, -1), -1, time.time_ns())

        # image_array = self.pre_process(image_array)
        contours = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(contours)
        contours = sorted(contours, key=cv2.contourArea, reverse=True)

        if len(contours) == 0:
            return None
        analysis_result.contours = contours
        largest_contour = contours[0]
        rows, cols = img.shape[:2]
        moments = cv2.moments(largest_contour)
        if moments['m00'] == 0:
            return None
        center = (int(moments['m10'] / moments['m00']), int(moments['m01'] / moments['m00']))
        analysis_result.center = center
        height_px = rows - center[1]
        analysis_result.height_px = height_px
        height_in_inches = self.height_px_to_in(height_px=height_px)
        analysis_result.height_in = height_in_inches

        _, _, w, _ = cv2.boundingRect(largest_contour)
        width_px = w
        analysis_result.width_px = width_px
        analysis_result.width_in = self.width_px_to_in(width_px=width_px, height_in=height_in_inches)
        

        topmost = tuple(largest_contour[largest_contour[:, :, 1].argmin()][0])
        analysis_result.maximum_point = topmost

        # Quantify roughness of the surface: 1.0 is very smooth, 0.0 is very rough
        area = cv2.contourArea(largest_contour)
        hull = cv2.convexHull(largest_contour)
        hull_area = cv2.contourArea(hull)
        
        # My understanding of solidity was off, it will increase as the contour gets more convex/concave rather than being a good representation of roughness
        # TODO: It was suggested that analyzing some sort of fft of the contour could be a good way to quantify roughness
        solidity = float(area) / hull_area
        analysis_result.roughness = solidity

        return analysis_result

    def correct_skew(self, image_array: np.array):
        # TODO: Do some skew correction here
        return image_array

    def isolate_red_mask(self, image_array: np.array) -> np.array:
        # Method obtained from:
        # https://stackoverflow.com/questions/32522989/opencv-better-detection-of-red-color/32523532#32523532
        # Range tuned with the included settings generator

        low_h, low_s, low_v = self.settings.low_hue, self.settings.low_sat, self.settings.low_val
        high_h, high_s, high_v = self.settings.high_hue, self.settings.high_sat, self.settings.high_val

        # Bitwise NOT to invert the image
        img = cv2.bitwise_not(image_array)

        # Convert to HSV
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

        # Create a mask for the red color
        mask = cv2.inRange(hsv, (low_h, low_s, low_v), (high_h, high_s, high_v))

        # Return the mask
        return mask

    def pre_process(self, image_array: np.array) -> np.array:
        img = image_array.copy()
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        img = self.correct_skew(img)

        redline_mask = self.isolate_red_mask(img)
        img = cv2.bitwise_and(img, img, mask=redline_mask)
        # img2 = cv2.cvtColor(img, cv2.COLOR_HSV2BGR)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        _, img = cv2.threshold(img, self.settings.threshold_min, self.settings.threshold_max, cv2.THRESH_BINARY)

        img = cv2.erode(img, self.kernel, iterations=self.settings.erosions)
        img = cv2.dilate(img, self.kernel, iterations=self.settings.dilations)

        return img
