import numpy as np
import cv2
import imutils
import math
import time
from picamera2 import Picamera2, MappedArray
from functools import reduce
import libcamera
from libcamera import controls
from enum import Enum
from pprint import pprint
from typing import *
from PIL import Image
from pathlib import Path
import json


class SensorModePriority(Enum):
    HighResolution = 1,
    HighFrameRate = 2,
    ClosestToRequestedResolution = 3,
    ClosestToRequestedFrameRate = 4


def save_image(image_array: np.array, metadata: dict = None):
    im = Image.fromarray(image_array)
    im = im.convert('RGB')
    im_path = Path.cwd() / 'images'
    im_path.mkdir(exist_ok=True)
    im_path /= f'{int(time.time_ns())}'
    im.save(im_path.with_suffix('.jpg'))
    print(f'Saved image to {im_path.with_suffix(".jpg")}')

    if metadata is not None:
        with open(im_path.with_suffix('.json'), 'w') as f:
            json.dump(metadata, f, indent=4, sort_keys=True)
        print(f'Saved metadata to {im_path.with_suffix(".json")}')


class Camera:
    def __init__(self, sensor_mode_priority=SensorModePriority.HighResolution, fps=30, resolution=(1920, 1080),
                 raw=False, raw_mode=None):
        self.camera = Picamera2()

        if sensor_mode_priority == SensorModePriority.HighResolution:
            # Find the largest resolution that the camera supports
            # self.camera.still_configuration.size = self.camera.sensor_resolution
            self.sensor_mode = reduce(
                lambda mode1, mode2: mode1 if reduce(
                    lambda x, y: x * y, mode1['size']) > reduce(lambda x, y: x * y,mode2['size']) else mode2,
                self.camera.sensor_modes
            )

        elif sensor_mode_priority == SensorModePriority.HighFrameRate:
            # Or maybe we want the highest framerate
            self.sensor_mode = max(self.camera.sensor_modes, key=lambda x: x['fps'])

        elif sensor_mode_priority == SensorModePriority.ClosestToRequestedResolution:
            # Or maybe we want the closest to the requested resolution
            self.sensor_mode = min(self.camera.sensor_modes,
                                   key=lambda x: abs(x['size'][0] - resolution[0]) + abs(x['size'][1] - resolution[1]))

        elif sensor_mode_priority == SensorModePriority.ClosestToRequestedFrameRate:
            # Or maybe we want the closest to the requested framerate
            self.sensor_mode = min(self.camera.sensor_modes, key=lambda x: abs(x['fps'] - fps))

        if raw and raw_mode is None:
            self.camera.still_configuration.enable_raw()
            raw_mode = {
                'size': self.camera.sensor_modes[0]['size'],
                'format': self.camera.sensor_modes[0]['format'],
            }
        elif not raw:
            raw_mode = None

        # Main Image Stream Settings
        self.main_stream = {
            'size': self.sensor_mode['size'],
        }

        # Do we want to treat this as a still or video? Default to still
        # Do we want to allow frame queueing? Default to yes
        self.config = self.camera.create_still_configuration(queue=True, main=self.main_stream, raw=raw_mode,
                                                             buffer_count=1)

        self.camera.align_configuration(self.config)
        self.camera.configure(self.config)

        self.default_controls = {
            'AnalogueGain': 1.0,
            'ColourGains': (1.0, 1.0),
            'Contrast': 1.0,
            'ExposureTime': 10000,
            'Saturation': 0.0,
            'Sharpness': 0.0
        }

        self.controls = {}

        self.set_controls(self.default_controls)
        self.camera.start()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()
        self.close()

    def start(self):
        self.camera.start()

    def stop(self):
        self.camera.stop()

    def close(self):
        self.camera.close()

    def set_controls(self, custom_controls: dict):
        for key, value in custom_controls.items():
            self.controls[key] = value
        self.camera.set_controls(self.controls)

    def clear_controls(self):
        self.controls = {}
        self.camera.set_controls(self.controls)

    def force_set_controls(self, custom_controls: dict):
        self.stop()

        self.set_controls(custom_controls)

        self.start()

    def get_normal_exposure_controls(self) -> dict:
        time.sleep(1)
        metadata = self.camera.capture_metadata()
        exposure_normal = metadata['ExposureTime']
        gain = metadata['AnalogGain'] * metadata['DigitalGain']
        # self.camera.stop()
        controls = {
            'ExposureTime': exposure_normal,
            'AnalogGain': gain,
        }
        capture_config = self.camera.create_still_configuration(queue=True, main=self.main_stream, controls=controls)
        # self.camera.configure(capture_config)
        # self.camera.start()
        return controls

    def get_short_exposure_controls(self) -> dict:
        normal_controls = self.get_normal_exposure_controls()
        short_exposure = int(normal_controls['ExposureTime'] / self.exposure_ratio)
        controls = {
            'ExposureTime': short_exposure,
            'AnalogGain': normal_controls['AnalogGain'],
        }
        return controls

    def get_long_exposure_controls(self) -> dict:
        normal_controls = self.get_normal_exposure_controls()
        long_exposure = int(normal_controls['ExposureTime'] * self.exposure_ratio)
        controls = {
            'ExposureTime': long_exposure,
            'AnalogGain': normal_controls['AnalogGain'],
        }
        return controls

    def set_post_callback(self, callback, stream='main', metadata=False):
        if metadata:
            self.set_post_callback_with_metadata(callback, stream)
            return

        def post_cb(request):
            with MappedArray(request, stream) as mapped:
                callback(mapped.array)

        self.camera.post_callback = post_cb

    def set_post_callback_with_metadata(self, callback, stream='main'):
        def post_cb(request):
            with MappedArray(request, stream) as mapped:
                callback(mapped.array, request.get_metadata())

        self.camera.post_callback = post_cb

    def set_pre_callback(self, callback, stream='main', metadata=False):
        if metadata:
            self.set_pre_callback_with_metadata(callback, stream)
            return

        def pre_cb(request):
            with MappedArray(request, stream) as mapped:
                callback(mapped.array)

        self.camera.pre_callback = pre_cb

    def set_pre_callback_with_metadata(self, callback, stream='main'):
        def pre_cb(request):
            with MappedArray(request, stream) as mapped:
                callback(mapped.array, request.get_metadata())

        self.camera.pre_callback = pre_cb
