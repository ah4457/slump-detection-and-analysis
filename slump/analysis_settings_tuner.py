from pathlib import Path
import cv2
import time
import imutils
from PIL import Image
import numpy as np
from typing import *
import os
from pprint import pprint
import random as rng
from dataclasses import dataclass, asdict, field
import json
from .analysis import AnalysisSettings, Kernel, SlumpAnalyzer
import argparse


def set_low_hue(_):
    global settings
    settings.low_hue = cv2.getTrackbarPos('Low Hue', trackbar_window)
    trackbar_cb()

def set_low_saturation(_):
    global settings
    settings.low_sat = cv2.getTrackbarPos('Low Sat', trackbar_window)
    trackbar_cb()

def set_low_value(_):
    global settings
    settings.low_val = cv2.getTrackbarPos('Low Val', trackbar_window)
    trackbar_cb()

def set_high_hue(_):
    global settings
    settings.high_hue = cv2.getTrackbarPos('High Hue', trackbar_window)
    trackbar_cb()

def set_high_saturation(_):
    global settings
    settings.high_sat = cv2.getTrackbarPos('High Sat', trackbar_window)
    trackbar_cb()

def set_high_value(_):
    global settings
    settings.high_val = cv2.getTrackbarPos('High Val', trackbar_window)
    trackbar_cb()

def set_threshold(_):
    global settings
    settings.threshold_min = cv2.getTrackbarPos('Thresh', trackbar_window)
    trackbar_cb()

def set_erode_iter(_):
    global settings
    settings.erosions = cv2.getTrackbarPos('Erosions', trackbar_window)
    trackbar_cb()

def set_dilate_iter(_):
    global settings
    settings.dilations = cv2.getTrackbarPos('Dilations', trackbar_window)
    trackbar_cb()

def set_kernel_mode(_):
    global settings
    settings.kernel_mode = cv2.getTrackbarPos('KMode', trackbar_window)
    trackbar_cb()

def set_kernel_size(_):
    global settings
    settings.kernel_size = cv2.getTrackbarPos('KSize', trackbar_window)
    trackbar_cb()



def display_image(window_name: str, image_array: np.array, width: int, height: int, blocking: bool = True):
    cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
    # if image is too big for the screen, resize it
    if image_array.shape[1] > width or image_array.shape[0] > height:
        image_array = imutils.resize(image_array, width=width, height=height)

    # resized_image = cv2.resize(image_array, fx=0.75, fy=0.75, dsize=(0, 0), interpolation=cv2.INTER_AREA)
    cv2.imshow(window_name, image_array)
    if blocking:
        cv2.waitKey(0)
        cv2.destroyWindow(window_name)


def color_gradient(start_color: Tuple, end_color: Tuple, steps: int):
    for i in range(steps):
        yield tuple([int(start_color[j] + (end_color[j] - start_color[j]) * i / steps) for j in range(3)])


def trackbar_cb():
    global settings, analyzer

    if settings is None:
        return

    if analyzer is None:
        return

    redline_mask = analyzer.isolate_red_mask(original.copy())
    redline = cv2.bitwise_and(original, original, mask=redline_mask)
    display_image(trackbar_intermediate_window, redline, 1024, 768, blocking=False)

    preprocess = analyzer.pre_process(original.copy())
    contours = cv2.findContours(preprocess, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    contours = imutils.grab_contours(contours)
    contours = sorted(contours, key=cv2.contourArea, reverse=True)
    if len(contours) > 0:
        drawing = original.copy()
        contour_colors = color_gradient((0, 255, 0), (255, 0, 0), len(contours))
        for i, contour in enumerate(contours):
            cv2.drawContours(drawing, [contour], 0, color=next(contour_colors), thickness=2)
            # break # only draw the largest contour

        display_image(trackbar_image_window, drawing, 1024, 768, blocking=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--image", type=str, default=None, required=True, help="The example image to use")
    parser.add_argument("--output", type=str, default=None, required=True, help="The output file to save the settings to")
    parser.add_argument("--settings", type=str, default=None, help="The existing settings file to use")
    args = parser.parse_args()

    image_path = Path(args.image)
    output_path = Path(args.output)
    settings_path = Path(args.settings)

    if not image_path.exists():
        print(f"Image file not found: {image_path}")
        exit(1)

    if settings_path is not None and not settings_path.exists():
        print(f"Settings file not found: {settings_path}")
        exit(1)

    if output_path.exists():
        print(f"Output file already exists: {output_path}")
        with open(output_path, 'r') as f:
            settings = AnalysisSettings.from_json(f.read())
            pprint(settings)
        print("If you continue, the existing settings will be used as the starting point unless a settings file was specified")
        print("Press enter to continue")
        input()
    else:
        settings = None

    original = cv2.imread(str(image_path))
    if original is None:
        print(f"Failed to load image: {image_path}")
        exit(1)

    if settings_path is not None:
        if settings is not None:
            print("Using provided settings file")
        with open(settings_path, 'r') as f:
            settings = AnalysisSettings.from_json(f.read())
            pprint(settings)
    elif settings is None:
        print("Using default settings")
        settings = AnalysisSettings()

    max_h = 180
    max_s = 255
    max_v = 255

    max_erosions = 10
    max_dilations = 10
    max_kernel_size = 10
    max_kernel_modes = 3

    trackbar_window = 'Settings'
    trackbar_image_window = 'Result'
    trackbar_intermediate_window = 'Intermediate'

    analyzer = SlumpAnalyzer()
    display_image(trackbar_image_window, original, 1024, 768, blocking=False)
    display_image(trackbar_intermediate_window, analyzer.isolate_red_mask(original), 1024, 768, blocking=False)
    cv2.namedWindow(trackbar_window, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(trackbar_window, 600, 600)

    cv2.createTrackbar('Low Hue', trackbar_window, settings.low_hue, max_h, set_low_hue)
    cv2.createTrackbar('High Hue', trackbar_window, settings.high_hue, max_h, set_high_hue)
    cv2.createTrackbar('Low Sat', trackbar_window, settings.low_sat, max_s, set_low_saturation)
    cv2.createTrackbar('High Sat', trackbar_window, settings.high_sat, max_s, set_high_saturation)
    cv2.createTrackbar('Low Val', trackbar_window, settings.low_val, max_v, set_low_value)
    cv2.createTrackbar('High Val', trackbar_window, settings.high_val, max_v, set_high_value)

    cv2.createTrackbar('Thresh', trackbar_window, settings.threshold_min, settings.threshold_max, set_threshold)
    cv2.createTrackbar('Erosions', trackbar_window, settings.erosions, max_erosions, set_erode_iter)
    cv2.createTrackbar('Dilations', trackbar_window, settings.dilations, max_dilations, set_dilate_iter)
    cv2.createTrackbar('KMode', trackbar_window, settings.kernel_mode, max_kernel_modes, set_kernel_mode)
    cv2.createTrackbar('KSize', trackbar_window, settings.kernel_size, max_kernel_size, set_kernel_size)

    trackbar_cb()

    # Wait until user press some key
    cv2.waitKey(0)
    cv2.destroyWindow(trackbar_window)
    cv2.destroyWindow(trackbar_image_window)
    cv2.destroyWindow(trackbar_intermediate_window)

    print("Saving settings to: " + str(output_path))
    pprint(settings)
    with open(output_path, 'w') as f:
        f.write(settings.to_json())
