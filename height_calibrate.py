from picamera2 import Picamera2, Preview, MappedArray

import numpy as np
import time
import math
import json
import imutils
import signal
import cv2

from slump.analysis import SlumpAnalyzer, AnalysisResult
from slump.camera import Camera, save_image
from gpiozero import LED
from PIL import Image
from pathlib import Path
from pprint import pprint
from libcamera import controls
from numpy.polynomial import Polynomial, Chebyshev, Legendre, Laguerre, Hermite, HermiteE
from matplotlib import pyplot as plt
from scipy.odr import Model, Data, ODR, RealData

import os
os.environ.pop("QT_QPA_PLATFORM_PLUGIN_PATH")
# os.environ["QT_QPA_PLATFORM"] = "wayland"

def null_cb(request):
    return

def display_cb(request):
    global s_analyzer
    if s_analyzer is None:
        return
    
    with MappedArray(request, "main") as main:
        # to_save = cv2.cvtColor(main.array, cv2.COLOR_BGR2RGB)
        # save_image(to_save)
        pre_proc = s_analyzer.pre_process(main.array)
        anal_res = s_analyzer.single_image_analysis(pre_proc)
        if anal_res is None:
            return
        
        cv2.drawContours(main.array, [anal_res.contours[0]], -1, (0, 255, 0), 3)
        cv2.circle(main.array, anal_res.center, 10, (255, 255, 0), -1)
        
        font = cv2.FONT_HERSHEY_SIMPLEX
        text_color = (255, 255, 255)
        font_scale = 1
        text_thickness = 2
        text_height_spacing = 50
        text_height_start = 500
        
        texts = [
            f"Timestamp (ns): {anal_res.timestamp}",
            f"Height (px): {anal_res.height_px}",
            f"Height (in): {anal_res.height_in:0.3f}",
            f"Width (px): {anal_res.width_px}",
            f"Width (in): {anal_res.width_in:0.3f}",
            f"Roughness: {anal_res.roughness:0.3f}",
            f"Height Mode: {s_analyzer.height_mode}",
            f"Height Equation: {s_analyzer[height_modes[s_analyzer.height_mode]]:ascii}"
        ]
        
        for i, text in enumerate(texts):
            cv2.putText(main.array, text, (0, text_height_start + i * text_height_spacing), font, font_scale, text_color, text_thickness)


def get_config(cam, main_stream, lores_stream):
    cam_controls = {
        'Brightness': -0.3,
        'ExposureTime': 13500,
    }
    
    preview_config = cam.create_preview_configuration(main=main_stream, lores=lores_stream, controls=cam_controls, buffer_count=1)
    return preview_config


def sig_handler(sig, frame):
    global cam, laser
    print("")
    print("Stopping camera...")
    if cam is not None:
        cam.stop()
        # cam.stop_preview()
    
    print("Turning off laser...")
    if laser is not None:
        laser.off()
        
    print("Exiting...")
    exit(0)


def get_px_and_in_arrays(cam, s_analyzer):
    pixel_heights = []
    inch_heights = []
    
    while True:
        img = cam.capture_array("main")
       
        prep = s_analyzer.pre_process(img)
        
        result = s_analyzer.single_image_analysis(prep)
        if result is None:
            print("No result found. Try again.")
            continue
        px_h = result.height_px
        in_h = result.height_in
        
        print(f"Height: {in_h:0.3f} in")
        height_correct = input("Is the height correct? (y/N): ")
        
        if height_correct != "y" and height_correct != "Y":
            in_h = float(input("Enter the correct height in inches: "))
        
        pixel_heights.append(px_h)
        inch_heights.append(in_h)
        
        finished = input("Go to equation fitting? (y/N): ")
        if finished == "y" or finished == "Y":
            break
        
    return pixel_heights, inch_heights

height_modes = {
    'poly': 'height_poly',
    'cheby': 'height_cheby',
    'laguerre': 'height_laguerre',
    'legendre': 'height_legendre',
    'hermite': 'height_hermite',
    'hermiteE': 'height_hermiteE',
}

cam = None
laser = None
s_analyzer = None
def _main():
    global cam, laser, s_analyzer
    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTSTP, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)
    
    np.set_printoptions(precision=3)
    
    laser = LED(17)
    
    main_stream = {
        'size': (2592, 1944),
        'format': 'XRGB8888',
    }
    
    lores_stream = {
        # 'size': (320, 240),
        'size': (2592, 1944),
        'format': 'YUV420',
    }
    
    cam = Picamera2()
    conf = get_config(cam, main_stream, lores_stream)
    cam.align_configuration(conf)
    cam.configure(conf)
    cam.start_preview(Preview.QTGL, x=0, y=0, width=1920, height=1080)
    cam.start()
    laser.on()
    
    s_analyzer = SlumpAnalyzer(cam, main_stream['size'])
    cam.post_callback = display_cb
    
    eqn_objs = {
        'poly': Polynomial,
        'cheby': Chebyshev,
        'laguerre': Laguerre,
        'legendre': Legendre,
        'hermite': Hermite,
        'hermiteE': HermiteE,
    }
        
    
    while True:
        px_h, in_h = get_px_and_in_arrays(cam, s_analyzer)
        
        print(f"px: {px_h}")
        print(f"in: {in_h}")
        
        while True:
            eq_type = input("Enter the type of equation to fit (poly, cheby, laguerre, legendre, hermite, hermiteE): ")
            eq_type = eq_type.lower()
            if eq_type not in eqn_objs.keys():
                print("Invalid equation type, defaulting to poly")
                eq_type = "poly"
                
            poly_deg = int(input("Enter the degree of the polynomial to fit: "))
            poly = eqn_objs[eq_type].fit(px_h, in_h, poly_deg)
            anal_set = s_analyzer.get_settings()
            coefficients = poly.convert().coef
            anal_set[height_modes[eq_type]] = coefficients.tolist()
            s_analyzer.height_mode = eq_type
            s_analyzer.set_settings(anal_set)
            pprint(coefficients.tolist())
            
            finished = input("Finished with Polynomial Degrees? (y/N): ")
            if finished == "y" or finished == "Y":
                save_settings = input("Save settings? (y/N): ")
                if save_settings == "y" or save_settings == "Y":
                    s_analyzer.save_settings()
                break
        
        finished = input("Height Calibration finished? (y/N): ")
        if finished == "y" or finished == "Y":
            break
        
            
        
    cam.stop()
    laser.off()
    
    return

def tst(B, val):
    poly = Polynomial(B)
    return poly(val)

if __name__ == "__main__":
    _main()

