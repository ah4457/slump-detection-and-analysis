#!/usr/bin/python

from picamera2 import Picamera2, Preview, MappedArray
import cv2
import os
os.environ.pop("QT_QPA_PLATFORM_PLUGIN_PATH")   # This is needed to fix the conflict of opencv and picamera2's QTGL

import numpy as np
import time
import math
import json
import imutils
import signal

from slump.analysis import SlumpAnalyzer, AnalysisResult
from slump.camera import Camera, save_image
from gpiozero import LED
from PIL import Image
from pathlib import Path
from pprint import pprint
from libcamera import controls

def null_cb(request):
    pass

def display_cb(request):
    global s_analyzer, ref_img, laser_state
    if s_analyzer is None:
        return
    
    with MappedArray(request, "main") as main:
        # to_save = cv2.cvtColor(main.array, cv2.COLOR_BGR2RGB)
        # save_image(to_save)
        pre_proc = s_analyzer.pre_process(main.array)
        anal_res = s_analyzer.single_image_analysis(pre_proc)
        if anal_res is None:
            return
        
        cv2.drawContours(main.array, [anal_res.contours[0]], -1, (0, 255, 0), 3)
        cv2.circle(main.array, anal_res.center, 10, (255, 255, 0), -1)
        
        font = cv2.FONT_HERSHEY_SIMPLEX
        text_color = (255, 255, 255)
        font_scale = 3
        text_thickness = 4
        
        cv2.putText(main.array, f"Timestamp (ns): {anal_res.timestamp}", (0, 100), font, font_scale, text_color, text_thickness)
        cv2.putText(main.array, f"Height (px): {anal_res.height_px}", (0, 200), font, font_scale, text_color, text_thickness)
        cv2.putText(main.array, f"Height (in): {anal_res.height_in:0.3f}", (0, 300), font, font_scale, text_color, text_thickness)
        cv2.putText(main.array, f"Width (px): {anal_res.width_px}", (0, 400), font, font_scale, text_color, text_thickness)
        cv2.putText(main.array, f"Width (in): {anal_res.width_in:0.3f}", (0, 500), font, font_scale, text_color, text_thickness)
        cv2.putText(main.array, f"Roughness: {anal_res.roughness:0.3f}", (0, 600), font, font_scale, text_color, text_thickness)
        # cv2.putText(main.array, f"Height From Poly (in): {anal_res.height_poly:0.3f}", (0, 700), font, font_scale, text_color, text_thickness)
        

def get_config(cam, main_stream, lores_stream):
    cam_controls = {
        'Brightness': -0.3,
        'ExposureTime': 13500,
    }
    
    preview_config = cam.create_preview_configuration(main=main_stream, lores=lores_stream, controls=cam_controls, buffer_count=1)
    return preview_config


def sig_handler(sig, frame):
    global cam, laser
    print("")
    print("Stopping camera...")
    if cam is not None:
        cam.stop()
        # cam.stop_preview()
    
    print("Turning off laser...")
    if laser is not None:
        laser.off()
        
    print("Exiting...")
    exit(0)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTSTP, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)
    
    laser = LED(17)

    main_stream = {
        'size': (2592, 1944),
        'format': 'XRGB8888',
    }

    lores_stream = {
        # 'size': (320, 240),
        'size': (2592, 1944),
        'format': 'YUV420',
    }

    cam = Picamera2()
    conf = get_config(cam, main_stream, lores_stream)
    cam.align_configuration(conf)
    cam.configure(conf)
    cam.start_preview(Preview.QTGL, x=0, y=0, width=1920, height=1080)
    cam.start()

    s_analyzer = SlumpAnalyzer(cam, main_stream['size'])
    laser.on()
    cam.post_callback = display_cb
    
    # wait for user to quit
    while True:
        time.sleep(1)
    