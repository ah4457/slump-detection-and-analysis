import cv2
import numpy as np
import time
import os
import math
import json

from slump.camera import Camera, SensorModePriority, save_image
from slump.analysis import SlumpAnalyzer
from gpiozero import LED, PWMLED
from PIL import Image
from pathlib import Path
from pprint import pprint
from libcamera import controls

def null_cb(image_array: np.array):
    pass


slump_analyzer: SlumpAnalyzer = SlumpAnalyzer()
def save_pre_process(image_array: np.array, metadata: dict):
    global slump_analyzer
    if slump_analyzer is None:
        return

    processed = slump_analyzer.pre_process(image_array)
    save_image(processed, metadata)
    save_image(image_array, metadata)

def save_single_image_analysis(image_array: np.array, metadata: dict):
    global slump_analyzer
    if slump_analyzer is None:
        return

    processed = slump_analyzer.pre_process(image_array)
    # analyzed = slump_analyzer.single_image_analysis(processed)
    save_image(processed, metadata)
    save_image(image_array, metadata)


def test(image_array: np.array):
    print(image_array.shape)
    print('test')


def demo_cb(image_array: np.array, metadata: dict):
    global slump_analyzer
    if slump_analyzer is None:
        return

    original = image_array.copy()
    save_image(image_array.copy(), metadata)
    # redline_mask = slump_analyzer.isolate_red_mask(original)
    # redline = cv2.bitwise_and(original, original, mask=redline_mask)
    # redline = cv2.cvtColor(redline, cv2.COLOR_HSV2BGR)
    # save_image(redline, metadata)
    # cv2.imwrite('./images/redline.jpg', 255*redline)

    # processed = slump_analyzer.pre_process(original)
    # save_image(processed, metadata)

    # analysis = slump_analyzer.single_image_analysis(processed)

    # pprint(analysis)
    # print()





def set_demo_camera_controls(camera: Camera):
    cam_controls = {
        'AeEnable': False,
        'AwbEnable': False,
        'AnalogueGain': 1.0,
        'Brightness': -0.3,
        'ColourGains': (4.0, 1.0),
        'Contrast': 1.0,
        'ExposureTime': 13500,
        # 'ExposureTime': 30000,
        'ExposureValue': -4.0,
        'Saturation': 1.0,
        'Sharpness': 1.0,
        'NoiseReductionMode': controls.draft.NoiseReductionModeEnum.Off
    }

    camera.set_controls(cam_controls)

    print("Setting camera controls to:")
    pprint(cam_controls)
    time.sleep(3.0)


def demo():
    with Camera(sensor_mode_priority=SensorModePriority.HighFrameRate, raw=False) as demo_cam:
        global slump_analyzer

        laser = LED(17)
        laser.on()

        slump_analyzer = SlumpAnalyzer(demo_cam.camera, demo_cam.main_stream['size'])
        set_demo_camera_controls(demo_cam)
        demo_cam.set_pre_callback(demo_cb, stream='main', metadata=True)

        laser.off()

def main():
    with Camera(sensor_mode_priority=SensorModePriority.HighResolution, raw=False) as camera:
        global slump_analyzer

        laser = LED(17)
        laser.on()

        slump_analyzer = SlumpAnalyzer(camera.camera, camera.main_stream['size'])
        set_test_default_controls(camera)
        camera.set_pre_callback(save_single_image_analysis, stream='main', metadata=True)

        laser.off()

    print(f"Test complete. Photos saved to {Path.cwd() / 'images'}")


steps = np.linspace(0, 1, num=2)
step_num = 0
num_fails = 0
allowed_fails = 10
control_par_str = 'Sharpness'
should_save = False
increment_step = False
def test_control_param_cb(image_array: np.array, metadata: dict):
    global step_num, control_par_str, should_save, steps, num_fails, allowed_fails, increment_step
    if not should_save:
        return
    close_enough = 0.1
    if isinstance(steps[step_num], tuple) and not math.isclose(metadata[control_par_str][0], steps[step_num][0], abs_tol=close_enough) and not math.isclose(metadata[control_par_str][1], steps[step_num][1], abs_tol=close_enough) and num_fails < allowed_fails:
        num_fails += 1
        print(f"\tStep {step_num} - {control_par_str} is {metadata[control_par_str]}, should be {steps[step_num]}")
        print("\tTrying again...")
        return
    elif isinstance(steps[step_num], int) or isinstance(steps[step_num], float) and not math.isclose(steps[step_num], metadata[control_par_str], rel_tol=close_enough) and num_fails < allowed_fails:
        num_fails += 1
        print(f"\tStep {step_num} - {control_par_str} is {metadata[control_par_str]}, should be {steps[step_num]}")
        print("\tTrying again...")
        return
    elif num_fails >= allowed_fails:
        print("\tToo many fails, saving anyway and moving on")
    should_save = False
    im = Image.fromarray(image_array)
    im_path = Path.cwd() / 'images'
    im_path.mkdir(exist_ok=True)
    im_path /= f'{int(metadata["SensorTimestamp"])}-Step-{step_num}-{control_par_str}-{metadata[control_par_str]}'
    im.save(im_path.with_suffix('.jpg'))
    print(f'Saved image to {im_path.with_suffix(".jpg")}')

    with open(im_path.with_suffix('.json'), 'w') as f:
        json.dump(metadata, f, indent=4, sort_keys=True)

    # pprint(metadata)
    # step_num += 1
    increment_step = True
    num_fails = 0


def test_control_param(control_par, step_count, camera: Camera, manual_min=None, manual_max=None):
    global step_num, control_par_str, should_save, steps, increment_step
    print("")
    laser = LED(17)

    should_save = False
    step_num = 0
    previous_step = -1
    camera.set_pre_callback(null_cb)
    laser.on()

    set_test_default_controls(camera)

    mini, maxi, defi = camera.camera.camera_controls[control_par]
    curi = mini

    if manual_min is not None:
        mini = manual_min
    if manual_max is not None:
        maxi = manual_max

    print(f"Testing {control_par}:")
    print(f"\tMin: {mini}, Max: {maxi}, Default: {defi}")
    steps = np.linspace(mini, maxi, num=step_count)
    if control_par == 'ExposureTime':
        steps = np.linspace(mini, maxi, num=step_count, dtype=int)
    if control_par == 'ColourGains':
        steps = np.linspace(mini, maxi, num=step_count)
        # create array of tuples
        steps = [(x, 0.0) for x in steps] + [(0.0, x) for x in steps]
    print(f"\tSteps: {steps}")

    control_par_str = control_par
    camera.set_controls({control_par: curi})
    camera.set_pre_callback(test_control_param_cb, stream='main', metadata=True)
    while step_num < step_count:
        camera.set_pre_callback(null_cb)
        curi = steps[step_num]
        camera.set_pre_callback(test_control_param_cb, stream='main', metadata=True)

        camera.set_controls({control_par: curi})
        if increment_step:
            step_num += 1
            increment_step = False
        if step_num != previous_step:
            time.sleep(0.1)
            should_save = True
            previous_step = step_num

    camera.set_pre_callback(null_cb)
    camera.set_controls({control_par: defi})
    laser.off()
    print("\n")


def set_test_default_controls(camera: Camera):
    # camera.stop()
    camera.set_controls({'AeEnable': False})
    camera.set_controls({'AwbEnable': False})
    camera.set_controls({'AnalogueGain': 1.0})
    camera.set_controls({'Brightness': -0.3})
    camera.set_controls({'ColourGains': (4.0, 1.0)})
    camera.set_controls({'Contrast': 1.0})
    camera.set_controls({'ExposureTime': 13500})
    camera.set_controls({'ExposureValue': -4.0})
    camera.set_controls({'Saturation': 1.0})
    camera.set_controls({'Sharpness': 1.0})
    camera.set_controls({'NoiseReductionMode': controls.draft.NoiseReductionModeEnum.Off})
    # camera.start()

    time.sleep(3.0)

    pprint(camera.controls)
    pprint(camera.camera.controls.make_dict())



if __name__ == '__main__':
    demo()